class MoviesController < ApplicationController

	# GET /
	def index
		created_at_day = Time.now.utc - 86400
		@movies = Movie.order(user_count: :desc).order(created_at: :desc).where(:created_at.gt => created_at_day, :user_count.gt => 0).page(params[:page]).per(24)
		respond_to do |format|
			format.html
		end
	end

	# GET /movies/1
	def show
		@movie = Movie.find(params[:id])
		respond_to do |format|
			format.js
		end
	end

	# GET /auto
	def auto
		created_at_day = Time.now.utc - 86400
		@movies = Movie.order(user_count: :desc).order(created_at: :desc).where(:created_at.gt => created_at_day, :user_count.gt => 0).limit(100)
		respond_to do |format|
			format.js
		end
	end

end
