class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

	# エラー処理
  unless Rails.env.development?
	  rescue_from Exception, with: :render_500
	  rescue_from Mongoid::Errors::DocumentNotFound, with: :render_404
	  rescue_from ActionController::RoutingError, with: :render_404
	end

	# ルーティングエラー
	def routing_error
		raise ActionController::RoutingError.new(params[:not_found])
	end

	# NotFound
  def render_404(exception = nil)
    logger.error "Rendering 404 with exception: #{exception.message}" if exception
    logger.error exception.backtrace.join("\n") if exception
    respond_to do |format|
      format.html { render 'errors/error_404', status: 404 }
      # format.json { render json: { error: '404 error' }, status: 404 }
      # format.all { render nothing: true, status: 404 }
    end
  end

  # ErrorPage
  def render_500(exception = nil)
    logger.error "Rendering 500 with exception: #{exception.message}" if exception
    logger.error exception.backtrace.join("\n") if exception
    respond_to do |format|
      format.html { render 'errors/error_500', status: 500 }
      # format.json { render json: { error: '500 error' }, status: 500 }
      # format.all { render nothing: true, status: 500 }
    end
  end


end
