class ContactsController < ApplicationController

  # GET /contacts/new
	# お問い合わせ
	def new
		if params[:item_id]
			@item = Item.find(params[:item_id])
		end
		@contact = Contact.new
		respond_to do |format|
			format.html
		end
	end

	# POST /contacts
	# お問い合わせ登録
	def create
		@contact = Contact.new(contact_params)
		respond_to do |format|
			if params[:back]
				@contact.confirming = nil
				format.js {render 'admin/shared/form'}
				format.html {render :new}
			elsif @contact.save
				ContactMailer.create_notification_email(@contact).deliver_now
				ContactMailer.admin_create_notification_email(@contact).deliver_now
				format.js
			else
				format.js {render 'admin/shared/form'}
				format.html {render :new}
			end
		end
	end


	private

	def contact_params
		params.require(:contact).permit(:email, :name, :description, :confirming)
  end
  
end
