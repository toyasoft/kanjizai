module ApplicationHelper

	#メタタグの設定
	def default_meta_tags
		{
			site: "インターネットテレビくん - KANJIZAITV -",
			reverse: true,
			# title: "",
			description: "インターネットテレビくん - KANJIZAITV - は話題のYouTube動画をリアルタイムにランキングするウェブサービスです。今巷で話題のYouTube動画をいち早くチェックしよう。Twitterでは日刊ランキングを配信中！",
			keywords: "KANJIZAITV, インターネットテレビくん,インターネットテレビ, ミュージックビデオ, 動画, ランキング, 話題",
			canonical: "https://kanjizai.tv",
			og: {
				title: :title,
				url: "https://kanjizai.tv",
				image: image_url('logo.png'),
				site_name: "インターネットテレビくん - KANJIZAITV -",
				description: :description,
				locale: 'ja_JP'
			},
			twitter: {
				card: 'summary',
				site: '@kanjizaitv'
			}
		}
	end

	# キーワード取得
  def get_keywords(title)
    keywords = []
    keywords << %W(#{title} インターネットテレビくん KANJIZAITV Twitter 動画 YouTube)
  end
	
end
