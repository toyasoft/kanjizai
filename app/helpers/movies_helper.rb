module MoviesHelper

	#経過時間の表示
	def time_diff(time)
		diff = Time.now - time
		day_diff = (diff / 86400).to_i
		if day_diff >= 7
			if day_diff / 7 < 5
				return ((day_diff / 7).to_i).to_s + t("helpers.time_diff.weeks_ago")
			else
				return time.strftime("%-m月%-d日")
			end
		else
			if day_diff >= 1
				if day_diff == 1
					return t("helpers.time_diff.yesterday")
				else
					return ((day_diff).to_i).to_s + t("helpers.time_diff.days_ago")
				end
			else
				if diff >= 3600
					return ((diff / 3600).to_i).to_s + t("helpers.time_diff.hours_ago")
				elsif diff >= 120
					return ((diff / 60).to_i).to_s + t("helpers.time_diff.minuts_ago")
				else
					return t("helpers.time_diff.just_now")
				end
			end
		end
	end
	
end
