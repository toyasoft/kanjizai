require "net/https"

class Movie
  include Mongoid::Document

  field :title, type: String
  field :movie_id, type: String
  field :user_count, type: Integer
  field :url, type: String
  field :image, type: String
  field :created_at, type: Time
  field :provide_name, type: String
  field :user_name, type: String
  field :profile_image_url, type: String
  field :provide_id, type: String
  field :body, type: String

  index({movie_id: 1}, {unique: true, name: "movie_index"})

  # YOUTUBE動画IDを取得
  def self.get_movie_id(feed)
    feed.urls.each do |url|
      location = url.expanded_url.to_s
      case location
      when /youtube\.com\/watch\?v=/
        movie_id = location.gsub(/^.*v=([A-Za-z0-9_-]+).*/, '\1')[0..10]
      when /youtu\.be/
        movie_id = location.gsub(/^.*youtu\.be\/([A-Za-z0-9_-]+).*/, '\1')[0..10]
      else
        # 何もしない
      end
      if movie_id.present?
        Movie.create_from_twitter(movie_id, feed)
      end
    end
  end

  def self.create_from_twitter(movie_id, feed)
    begin
      # API問い合わせ
      uri = URI.parse("https://www.googleapis.com/youtube/v3/videos?id=#{movie_id}&key=#{ENV['GOOGLE_API_KEY']}&fields=items(snippet(title,thumbnails))&part=snippet")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      req = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(req)
      json = response.body
    rescue => e
      # ログの出力
      logger.error e
      logger.error e.backtrace.join("\n")
    else
      # データを保存
      created_at_day = Time.now.utc - 86400
      user_count = 1
      user_count += Movie.only(:id).desc(:created_at).where(movie_id: movie_id, :created_at.gt => created_at_day).count.to_i

      if hash = JSON.parse(json.force_encoding("UTF-8"))
        unless hash['items'].empty? && hash['items'][0].blank?
          movies = Movie.where(movie_id: movie_id, :created_at.gt => created_at_day, :user_count.gt => 0)
          movies.update_all(user_count: 0) if movies.present?
          old_movies = Movie.where(movie_id: movie_id, :created_at.lt => created_at_day)
          old_movies.destroy
          title = hash['items'][0]['snippet']['title'].presence || nil
          image = hash['items'][0]['snippet']['thumbnails']['medium']['url'].presence || nil
          @movie = Movie.new(user_count: user_count, movie_id: movie_id, title: title, image: image, url: "http://www.youtube.com/watch?v=#{movie_id}", created_at: feed.created_at.to_time, provide_name: feed.user.screen_name, user_name: feed.user.name, profile_image_url: feed.user.profile_image_url_https, provide_id: feed.id, body: feed.text)
          @movie.save
        end
      end
    end
  end

end