// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require masonry.pkgd
//= require infinite-scroll.pkgd
//= require imagesloaded.pkgd
//= require jquery.lazyload.min
//= require headroom.min
//= require jQuery.headroom
//= require_tree .

$(document).on('turbolinks:load', function() {

	var $grid = $('.movies').masonry({
		isFitWidth: true,
	  itemSelector: 'none',
	  columnWidth: '.grid__col-sizer',
	  gutter: '.grid__gutter-sizer',
	  // percentPosition: true,
	  stagger: 30,
	  visibleStyle: { transform: 'translateY(0)', opacity: 1 },
	  hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },
	});

	var msnry = $grid.data('masonry');

	$grid.imagesLoaded( function() {
	  // $grid.removeClass('are-images-unloaded');
	  $grid.masonry( 'option', { itemSelector: '.movie' });
	  var $items = $grid.find('.movie');
	  $grid.masonry( 'appended', $items );
	});

	$grid.infiniteScroll({
	  path: 'nav.pagination a[rel=next]',
	  append: '.movie',
	  outlayer: msnry,
	  hideNav: '.pagination',
	  status: '.page-load-status',
	});

	$(".headroom").headroom({
		offset: 52,
	});
});

// モーダルウィンドウのセンタリング
function centeringModalSyncer() {
	var w = window.innerWidth;
	var h = window.innerHeight;
	var cw = $('#modal-content').outerWidth();
	var ch = $('#modal-content').outerHeight();
	var pxleft = ((w - cw) / 2);
	var pxtop = ((h - ch) / 2);

	$('#modal-content').css({'left': pxleft + 'px', 'top': pxtop + 'px'});
};

// モーダルウィンドウ開く
function openModalWindow() {
	// start_loading();
	$("#modal-overlay, #modal-content").show();
	// stop_loading(spinner);
	// モーダルコンテンツを画面の中央へ
	centeringModalSyncer();
	// $('#modal-content').imagesLoaded(function(){
	// 	centeringModalSyncer();
	// });
	$(window).on('resize', function(){
		centeringModalSyncer();
	});
	// モーダルウィンドウ閉じる
	$('#modal-close, #modal-overlay').unbind().on('click', function(e){
		if (e.target !== e.delegateTarget) return;
		// $('#modal-content, #modal-overlay').fadeOut('slow',function(){
			$('#modal-overlay').remove();
		// });
		return false;
	});
};

// ローディングスタート
function start_loading() {
  var opts, target;
  opts = {
    lines: 13,
    length: 15,
    width: 8,
    radius: 18,
    corners: 1,
    rotate: 0,
    direction: 1,
    color: '#fff',
    speed: 1,
    trail: 60,
    shadow: false,
    hwaccel: false,
    className: 'spinner',
    zIndex: 2e9,
    position: "fixed"
  };
  target = $('body')[0];
  spinner = new Spinner(opts).spin(target);
  $(target).data('spinner', spinner);
};

// ローディングストップ
function stop_loading(spinner) {
  spinner.spin(false);
};