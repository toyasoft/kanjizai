$(document).on 'turbolinks:load', ->
  if window.ga != undefined
    ga('set', 'location', location.href.split('#')[0])
    ga('send', 'pageview')