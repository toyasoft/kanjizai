class MovieJob < ApplicationJob
	require "net/http"
  queue_as :default

  def perform(movie_id, feed)
  	Movie.create_from_twitter(movie_id, feed)
  end
end
