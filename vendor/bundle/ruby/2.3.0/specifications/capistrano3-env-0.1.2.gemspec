# -*- encoding: utf-8 -*-
# stub: capistrano3-env 0.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "capistrano3-env"
  s.version = "0.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Miguel Palhas"]
  s.date = "2016-07-20"
  s.description = "Environment variables management for Capistrano 3"
  s.email = ["mpalhas@gmail.com"]
  s.executables = ["cap-env"]
  s.files = ["bin/cap-env"]
  s.homepage = "https://github.com/naps62/capistrano-env"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "Environment variables management for Capistrano 3"

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<capistrano>, ["~> 3.1"])
    else
      s.add_dependency(%q<capistrano>, ["~> 3.1"])
    end
  else
    s.add_dependency(%q<capistrano>, ["~> 3.1"])
  end
end
