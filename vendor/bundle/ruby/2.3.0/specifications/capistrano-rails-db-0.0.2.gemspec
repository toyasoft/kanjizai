# -*- encoding: utf-8 -*-
# stub: capistrano-rails-db 0.0.2 ruby lib

Gem::Specification.new do |s|
  s.name = "capistrano-rails-db"
  s.version = "0.0.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Kentaro Imai"]
  s.date = "2015-03-19"
  s.description = "Rails migration tasks for Capistrano v3"
  s.email = ["kentaroi@gmail.com"]
  s.homepage = "https://github.com/kentaroi/capistrano-rails-db"
  s.rubygems_version = "2.5.1"
  s.summary = "Rails migration tasks for Capistrano v3"

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<capistrano-rails>, ["~> 1.1"])
    else
      s.add_dependency(%q<capistrano-rails>, ["~> 1.1"])
    end
  else
    s.add_dependency(%q<capistrano-rails>, ["~> 1.1"])
  end
end
