# -*- encoding: utf-8 -*-
# stub: capistrano-memcached 1.2.0 ruby lib

Gem::Specification.new do |s|
  s.name = "capistrano-memcached"
  s.version = "1.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Ruben Stranders"]
  s.date = "2015-06-06"
  s.description = "Capistrano tasks for automatic and sensible memcached configuration.\nEnables zero downtime deployments of Rails applications. Configs can be\ncopied to the application using generators and easily customized.\nWorks *only* with Capistrano 3+.\nHeavily inspired (i.e. copied and reworked) by https://github.com/bruno-/capistrano-unicorn-nginx\n"
  s.email = ["r.stranders@gmail.com"]
  s.homepage = "https://github.com/rhomeister/capistrano-memcached"
  s.rubygems_version = "2.5.1"
  s.summary = "Capistrano tasks for automatic and sensible memcached configuration."

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<capistrano>, [">= 3.1"])
      s.add_runtime_dependency(%q<sshkit>, [">= 1.2.0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
    else
      s.add_dependency(%q<capistrano>, [">= 3.1"])
      s.add_dependency(%q<sshkit>, [">= 1.2.0"])
      s.add_dependency(%q<rake>, [">= 0"])
    end
  else
    s.add_dependency(%q<capistrano>, [">= 3.1"])
    s.add_dependency(%q<sshkit>, [">= 1.2.0"])
    s.add_dependency(%q<rake>, [">= 0"])
  end
end
