(function() {
  $(document).on('turbolinks:load', function() {
    if (window.ga !== void 0) {
      ga('set', 'location', location.href.split('#')[0]);
      return ga('send', 'pageview');
    }
  });

}).call(this);
