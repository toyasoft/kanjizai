FROM ruby:2.4.9
RUN apt-get update -qq && apt-get install -y nodejs
RUN mkdir /kanjizai
WORKDIR /kanjizai
COPY Gemfile /kanjizai/Gemfile
COPY Gemfile.lock /kanjizai/Gemfile.lock
RUN bundle install
COPY . /kanjizai

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]