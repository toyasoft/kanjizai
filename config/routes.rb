Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # トップページ
  root to: %(movies#index)

  resources :movies do
  	collection do
  		get :auto
  	end
  end

  # ポリシー
  namespace :policies do
    resources :terms, only: [:index]
    resources :privacy, only: [:index]
  end

  # エラー
  get '*not_found' => 'application#routing_error'
  post '*not_found' => 'application#routing_error'
end
