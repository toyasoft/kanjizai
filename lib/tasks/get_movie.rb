class Tasks::GetMovie
	def self.create

		# TwitterAPIトークン
		client = Twitter::REST::Client.new do |config|
      config.consumer_key = ENV["TWITTER_CONSUMER_KEY"]
      config.consumer_secret = ENV["TWITTER_CONSUMER_SECRET"]
      config.access_token = ENV["TWITTER_ACCESS_TOKEN"]
      config.access_token_secret = ENV["TWITTER_ACCESS_TOKEN_SECRET"]
    end

		# 最後の動画
		last_movie = Movie.order(created_at: :asc).last

		# Tweetを取得
		if last_movie.present? && last_movie.provide_id != 0
			client.home_timeline(count: 200, since_id: last_movie.provide_id, include_entities: true).reverse.each do |feed|
				Movie.get_movie_id(feed) unless feed.user.screen_name == 'kanjizaitv' || feed.urls.blank?
			end
		else
			client.home_timeline(count: 200, include_entities: true).reverse.each do |feed|
				Movie.get_movie_id(feed) unless feed.user.screen_name == 'kanjizaitv' || feed.urls.blank?
			end
		end

	end
end