class Tasks::BotTwitter

	def self.bot_twitter
		client = Twitter::REST::Client.new do |config|
		  config.consumer_key        = ENV["TWITTER_CONSUMER_KEY"]
		  config.consumer_secret     = ENV["TWITTER_CONSUMER_SECRET"]
		  config.access_token        = ENV["TWITTER_ACCESS_TOKEN"]
		  config.access_token_secret = ENV["TWITTER_ACCESS_TOKEN_SECRET"]
		end
		created_at_day = Time.now.utc - 86400
	  movie = Movie.order(user_count: :desc).order(created_at: :desc).where(:created_at.gt => created_at_day, :user_count.gt => 0).first
	  client.update("本日のランキング1位: #{movie.title[0..70]} https://youtu.be/#{movie.movie_id} @kanjizaitv https://kanjizai.tv")
  end
  
end